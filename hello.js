var express = require('express');
var app 	= express();
var pg = require('pg');

var connectionString = "postgresql://administrator:1111@localhost:5432/postgres";
var pgClient = new pg.Client(connectionString);


const KrakenClient = require('kraken-api');
const kraken       = new KrakenClient();

app.get('/', function (req, res){
	res.send('Hello World!');
})

app.get('/rates', function (req, res){
	(async () => {

		await kraken.api('Ticker', {"pair": 'XBTUSD, ETHUSD, XBTEUR, ETHEUR, ETHXBT'}, function(error, data) {
	   		if(error) {
	        	console.log(error);
	        	res.send('Error')
	    	}
	    	else {
	        	console.log(data.result);
	        	res.send(data.result)
				pgClient.connect();
  				pgClient.query("INSERT INTO test(data) VALUES ($1)", [data]);
	    	}
		});
	})();
})
app.listen(8080, function(){
	console.log('Hello World is Started!')
})

